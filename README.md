# Intro

Designed to run 3 brushed dc motors with pwm signals and get feedback from 3 optical encoders while being commanded over spi as a slave

# Hardware list 

| part number                             | notes                                                                            |
| --------------------------------------- | -------------------------------------------------------------------------------- |
| Nucleo-H743ZI2                          | system controler                                                                 |
| LPD3806-400bm-G5-24C                    | Optical encoder 400 pulse per revolution from [ali express]()                    | 
| MC33926 Motor Driver Carrier            | [Pololu #1212](https://www.pololu.com/product/1213)                              | 
| 12V/17.3Kg-cm/18RPM 139:1 DC Gear Motor | [brushed dc motor](https://www.phidgets.com/?tier=3&catid=19&pcid=16&prodid=295) |

## Pinout

| Position | Name                    | Type    | Signal          | Label                        | [CN11 Pin](https://www.st.com/resource/en/user_manual/dm00244518-stm32-nucleo144-boards-stmicroelectronics.pdf) | CN12 Pin |
| -------- | ----------------------- | ------- | --------------- | ---------------------------- | --------------------------------------------------------------------------------------------------------------- | -------- |
| 7        | PC13                    | Input   | GPIO_Input      | B1 [Blue PushButton]         | 23                                                                                                              | N/A      |
| 46       | PB0                     | Output  | GPIO_Output     | LD1 [Green Led]              | 34                                                                                                              | N/A      |
| 75       | PB14                    | Output  | GPIO_Output     | LD3 [Red Led]                | N/A                                                                                                             | 28       |
| 142      | PE1                     | Output  | GPIO_Output     | LD2 [Yellow Led]             | 61                                                                                                              | N/A      |
| **60**   | **PE9**                 | **I/O** | **TIM1_CH1**    | **Encoder 1 A**              | **N/A**                                                                                                         | **52**   |
| **64**   | **PE11**                | **I/O** | **TIM1_CH2**    | **Encoder 1 B**              | **N/A**                                                                                                         | **56**   |
| **34**   | **PA0**                 | **I/O** | **TIM2_CH1**    | **Encoder 2 A**              | **28**                                                                                                          | **N/A**  |
| **133**  | **PB3 (JTDO/TRACESWO)** | **I/O** | **TIM2_CH2**    | **Encoder 2 B**              | **N/A**                                                                                                         | **31**   |
| **42**   | **PA6**                 | **I/O** | **TIM3_CH1**    | **Encoder 3 A**              | **N/A**                                                                                                         | **13**   |
| **97**   | **PC7**                 | **I/O** | **TIM3_CH2**    | **Encoder 3 B**              | **N/A**                                                                                                         | **19**   | 
| *139*    | *PB8*                   | *I/O*   | *TIM4_CH1*      | *Motor 1 PWM output*         | *N/A*                                                                                                           | *3*      |
| *136*    | *PB6*                   | *I/O*   | *TIM4_CH2*      | *Motor 2 PWM output*         | *N/A*                                                                                                           | *17*     |
| *137*    | *PB7*                   | *I/O*   | *TIM4_CH3*      | *Motor 3 PWM output*         | *21*                                                                                                            | *N/A*    | 
| **96**   | **PC6**                 | **I/O** | **GPIO_Output** | **Motor 1 direction output** | **N/A**                                                                                                         | **4**    |
| **98**   | **PC8**                 | **I/O** | **GPIO_Output** | **Motor 2 direction output** | **N/A**                                                                                                         | **2**    |
| **99**   | **PC9**                 | **I/O** | **GPIO_Output** | **Motor 3 direction output** | **N/A**                                                                                                         | **1**    | 
| 41       | PA5                     | Input   | SPI1_SCK        | SPI clock                    | N/A                                                                                                             | 11       |
| 123      | PD7                     | Input   | SPI1_MOSI       | SPI Data in                  | 45                                                                                                              | N/A      |
| 124      | PG9                     | Output  | SPI1_MISO       | SPI Data out                 | 63                                                                                                              | N/A      |
| 40       | PA4                     | Input   | SPI1_NSS        | SPI Slave Select             | 32                                                                                                              | N/A      |
| 77       | PD8                     | I/O     | USART3_TX       | STLINK_RX                    | N/A                                                                                                             | 10       |
| 78       | PD9                     | I/O     | USART3_RX       | STLINK_TX                    | 69                                                                                                              | N/A      | 


# Notes

[good pictures of stm32 timer subsystem](https://deepbluembedded.com/stm32-pwm-example-timer-pwm-mode-tutorial/)